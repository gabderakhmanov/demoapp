package com.company.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by maxim on 6/1/17.
 * @author Maxim Gabderakhmanov
 * @version 1.0
 *
 */
public class Printer {

    public static void printFirstTenList(Map<String, Integer> map) {

        List<Map.Entry<String, Integer>> firstTenList = new ArrayList<Map.Entry<String, Integer>>(10);
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();

        for (int i = 0; iterator.hasNext() && i < 10; i++) {
            firstTenList.add(iterator.next());
        }

        System.out.println("Top 10 most common words in the file:");
        for(Map.Entry<String, Integer> item : firstTenList){
            System.out.println(item.getKey()+" ("+item.getValue()+")");
        }

    }

}
