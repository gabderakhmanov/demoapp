package com.company.service;

import java.io.*;

/**
 * Created by maxim on 6/1/17.
 * @author Maxim Gabderakhmanov
 * @version 1.0
 *
 */
public class Wellcommer {

    public static String getFileName(){

        Wellcommer.doWelcome();

        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String pathToFile = null;
        try {
            pathToFile = bufferedReader.readLine();
            System.out.println("The file: "+ pathToFile.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathToFile;
    }

    private static void doWelcome() {
        System.out.println("Please enter a path to your text file:");
    }

}
