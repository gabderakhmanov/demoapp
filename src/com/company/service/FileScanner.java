package com.company.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


/**
 * Created by maxim on 6/1/17.
 * @author Maxim Gabderakhmanov
 * @version 1.0
 *
 */
public class FileScanner {

    public static Map<String, Integer> doScanFile(String fileName) throws FileNotFoundException {

        Scanner txtFile = new Scanner(new File(fileName));
        txtFile.useDelimiter(",|\\s");

        Map<String, Integer> map = new HashMap<String, Integer>();

        while(txtFile.hasNext()) {
            String rawWord = txtFile.next();
            String word = Preparator.getPreparedString(rawWord).toLowerCase();

            if(word.length()>0){
                if(map.containsKey(word)){
                    int count = map.get(word) + 1;
                    map.put(word, count);
                } else {
                    map.put(word, 1);
                }
            }
        }

        txtFile.close();

        return map;
    }

}
