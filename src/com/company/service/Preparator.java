package com.company.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by maxim on 6/1/17.
 * @author Maxim Gabderakhmanov
 * @version 1.0
 *
 */
public class Preparator {

    public static String getPreparedString(String str){

        String pattern= "([a-zA-Z0-9-'])*";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(str);

        if (str!=null && str.length()>0 && m.matches()==false) {
            str = Preparator.cleanString(str);
        }

        return str;
    }

    private static String cleanString(String str) {
        for (int i = 0; i < patterns.length; i++) {
            str = patterns[i].matcher(str).replaceAll("");
        }
        return str;
    }

    private static final Pattern[] patterns = {
            Pattern.compile("^([^a-zA-Z0-9-'])*"),
            Pattern.compile("([^A-Za-z0-9]*$)"),
    };

}
