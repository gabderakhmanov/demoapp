package com.company.main;

import com.company.service.FileScanner;
import com.company.service.MapComparator;
import com.company.service.Printer;
import com.company.service.Wellcommer;

import java.io.*;
import java.util.*;

/**
 * Created by maxim on 6/1/17.
 * @author Maxim Gabderakhmanov
 * @version 1.0
 *
 * The application allows you to count the number of words in a text file.
 * Developed and tested in JDK 1.8 (java-8-oracle)
 *
 */
public class Main {

    public static void main(String[] args) throws IOException {

        String fileName = Wellcommer.getFileName();
        Map<String,Integer> map = FileScanner.doScanFile(fileName);

        Map<String,Integer> newMap = new TreeMap(new MapComparator(map));
        newMap.putAll(map);

        Printer.printFirstTenList(newMap);
    }

}
